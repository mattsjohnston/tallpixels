<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'catalyt1_tallpixels');

/** MySQL database username */
define('DB_USER', 'catalyt1_admin');

/** MySQL database password */
define('DB_PASSWORD', 'Fr0ntl1n3');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`w$1z<^7wTWkXJb+utqxa8ohE^+$| OSbeAx%(%TO/i.O&;G[-4$HWoS!o4Lh:vE');
define('SECURE_AUTH_KEY',  'pEkT$Iwxg7m/]E@(j|JLUUJ3+$oLM@o#uM/&RY2&32I;$Cihm|-%DZ*UMeBO(Qw^');
define('LOGGED_IN_KEY',    '3^Z_:| t)MJr|nphni|jJl&f@I|2t|rl+<~`>BbCTIi4|G[,Tt/`6j)Avk.r_1P5');
define('NONCE_KEY',        'N+Cn5$zY<Z=W4.MX1z*|; ,rj=@<fCNQ&Vg:gF0aOFYp,#5&=@-,)bx4t|{Ajo[C');
define('AUTH_SALT',        '=:0+4L|+4-+t<9/IF&j3GrA|B-QVO(j|RVt4nw8p>!+vQ#/b`<Fd#@ol2.qI/x/T');
define('SECURE_AUTH_SALT', '5W$W.0=-I4Y+w(RZ=.8%99g6`wgZ.mmd9Kc2XOZE^G5cf29_6(Yd=-fGY)a]5LMf');
define('LOGGED_IN_SALT',   '&T~w&C2XW|4>|c3e)|&Tj)M=|.,-U,y-vxY_]rFf^GJR<y?R*Jq`V}r_;2-^m/[z');
define('NONCE_SALT',       'w9z[~,~FB:d_z-nN@DHK-0(Qob#b!%>|Tl^vA8<-adXDfISN<_n369pLl2Q,Iuhq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
