<header id="banner" class="navbar navbar-static-top" role="banner">
  <div class="navbar-inner">
    <!-- <div class="container"> -->
      <!-- <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a> -->
      <a class="brand" href="<?php echo home_url(); ?>/">
        <?php bloginfo('name'); ?>
        <span class="cloud cloud1"><img src="/assets/img/cloud1.png" /></span>
        <span class="cloud cloud2"><img src="/assets/img/cloud2.png" /></span>
        <span class="cloud cloud3"><img src="/assets/img/cloud3.png" /></span>
        <span class="cloud cloud4"><img src="/assets/img/cloud4.png" /></span>
      </a>
      <p id="intro-line">Designing pixels and code since 2001</p>
      <!-- <nav id="nav-main" class="nav-collapse" role="navigation">
        <?php
          if (has_nav_menu('primary_navigation')) :
            wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav'));
          endif;
        ?>
      </nav> -->
      <ul id="social-icons">
        <li>
          <a href="mailto:matt@tallpixels.com" target="_blank" rel="nofollow" class="icon-mail" title="Email me"><span>Email me</span></a>
        </li>
        <li>
          <a href="http://dribbble.com/mattsjohnston" target="_blank" rel="nofollow" class="icon-dribbble" title="Find me on Dribbble"><span>Find me on Dribbble</span></a>
        </li>
        <li>
          <a href="http://github.com/mattsjohnston" target="_blank" rel="nofollow" class="icon-github" title="Find me on Github"><span>Find me on Github</span></a>
        </li>
        <li>
          <a href="http://twitter.com/mattsjohnston" target="_blank" rel="nofollow" class="icon-twitter" title="Find me on Twitter"><span>Find me on Twitter</span></a>
        </li>
        <li>
          <a href="http://feeds.feedburner.com/TallPixels" target="_blank" class="icon-rss" title="Subscribe to the RSS"><span>Subscribe to the RSS</span></a>
        </li>
      </ul>
    <!-- </div> -->
  </div> <!-- /.navbar-inner -->
</header>